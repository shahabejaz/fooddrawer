//
//  FoodItemCell.swift
//  FoodDrawer
//
//  Created by Shahab Ejaz on 06/09/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

import UIKit
import Messages
class FoodItemCell: UICollectionViewCell {
    @IBOutlet weak var stickerView: MSStickerView!
    func configure(usingImageName imageName:String) {
        guard let imagePath = Bundle.main.path(forResource: imageName, ofType: ".png") else {
            return
        }
        let path =  URL(fileURLWithPath: imagePath)
        do {
            let description = NSLocalizedString("Food Sticker", comment: "")
            let sticker = try MSSticker(contentsOfFileURL: path , localizedDescription: description)
            stickerView.sticker = sticker
        }
        catch {
            fatalError("Failed to create sticker: \(error)")
        }
    }
}
